<?php
/**
Plugin Name: MM Lead Capture
Description: Side bar lead capture form. Simple but flexible.
Author: Marketingmindz
Author URI: http://marketingmindz.com/
Version: 1.0.0
**/
if ( ! defined( 'ABSPATH' ) ) exit;	// Exit if accessed directly

if(!class_exists('ClassLeadCapture')) {
	class ClassLeadCapture{   		/**
		 * Construct The Plugin Object
		 *
		*/
		public function creating_custom_post()
	{
		if (!post_type_exists( LC_CUSTOM_POST_TYPE ) ) {
				register_post_type( LC_CUSTOM_POST_TYPE, array(
			   'labels' => array(
				'name' => __( 'LC Contact Form', 'contact_form' ),
				'singular_name' => __( 'LC Contact Form', 'contact_form' ) ),
			'rewrite' => false,
			'query_var' => false ) );
	    }
	}
		public function __construct() {
			/* Set the constants needed by the plugin. */
			add_action( 'plugins_loaded', array( &$this, 'lead_capture_constants' ), 1 );
			
			/* Load the admin files. */
			add_action( 'plugins_loaded', array( &$this, 'lead_capture_admin' ), 2 );	
			
			/* Add style for front-end */
			add_action( 'wp_enqueue_scripts', array(&$this, 'lead_capture_front_style'));

			/* Activation hook */
			register_activation_hook(__FILE__, array($this, 'lead_capture_activate'));
			
			/* Deactivation hook */
			register_deactivation_hook(__FILE__, array(&$this, 'lead_capture_deactivate'));
			
			/* Uninstall hook */
			register_uninstall_hook(__FILE__, array($this, 'lead_capture_uninstall'));
		}
		
		
		/**
		 * Defines constants used by the plugin.
		 *
		*/
		function lead_capture_constants(){			
			
			/* Set constant path to the LC plugin directory. */
			define( 'LEAD_CAPTURE_DIR', trailingslashit(plugin_dir_path( __FILE__ )));
	
			/* Set constant path to the LC plugin URL. */
			define( 'LEAD_CAPTURE_URL', trailingslashit(plugin_dir_url( __FILE__ )));			
	
			/* Set the constant path to the LC admin directory. */
			define( 'LEAD_CAPTURE_ADMIN', LEAD_CAPTURE_DIR . trailingslashit( 'admin' ) );				
			
			/* Set the constant path to the LC admin directory. */
			define( 'LEAD_CAPTURE_ADMIN_URL', LEAD_CAPTURE_URL . trailingslashit( 'admin' ) );
			
			/* Set the constant path to the LC stylesheet directory. */
			define( 'LEAD_CAPTURE_CSS', LEAD_CAPTURE_URL . trailingslashit( 'css' ));

			/* Set the constant path to the LC script directory. */
			define( 'LEAD_CAPTURE_JS', LEAD_CAPTURE_URL . trailingslashit( 'js' ));
			
			/* Set the constant path to the LC Image directory. */
			define( 'LEAD_CAPTURE_IMAGE', LEAD_CAPTURE_URL . trailingslashit( 'images' ));
			/*Custom Post type*/
			define('LC_CUSTOM_POST_TYPE', 'lc_setting' );

			/* Set the constant path to the LC Image directory. */
			define( 'LEAD_CAPTURE_SHORTCODE', LEAD_CAPTURE_DIR . trailingslashit( 'shortcode' ));
				
		}
		/**
		 * Loads the admin section files needed by the plugin.
		 *
		**/
		function lead_capture_admin() {
			/* Load the Files in Admin Section. */			
			require_once( LEAD_CAPTURE_ADMIN . 'functions.php');
			require_once(LEAD_CAPTURE_SHORTCODE.'contactformshortcode.php');
			wp_enqueue_style('lc_admin_style',LEAD_CAPTURE_CSS.'styles.css');						
			wp_enqueue_script('lc_admin_script',LEAD_CAPTURE_JS.'scripts.js');
					
		}
		
		/**
		 *  Enqueue stylesheet For Front End
		 *
		**/
		function lead_capture_front_style() {
			wp_enqueue_style('lc_front_style',LEAD_CAPTURE_CSS.'slider.css');			
			wp_enqueue_script('lc_front_script',LEAD_CAPTURE_JS.'slider.js');
			echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>';
			wp_enqueue_script('lc_front_script_form',LEAD_CAPTURE_JS.'formvalidation.js');
			wp_enqueue_script('lc_front_script_captcha',LEAD_CAPTURE_JS.'captcha.js');
			/*wp_enqueue_script('lc_front_script_form',LEAD_CAPTURE_JS.'jquery.form-validator.min.js');	*/		
					}
		
		/**
		 *  Activate the plugin
		 *
		**/
		function lead_capture_activate() {
			// Create post object
		$my_post = array(
		  'post_title'    => 'MM Contact Form 1',
		  'post_content'  => 'This is first contact form.',
		  'post_status'   => 'publish',
		  'post_type' => LC_CUSTOM_POST_TYPE
		);
		// Insert the post into the database
		wp_insert_post( $my_post );	

		}

		/**
		 *   deactivate Hook Of The Plugin
		 *
		**/		
		function lead_capture_deactivate() {
			 
		}
		/**
		 *   Uninstall Hook Of The Plugin
		 *
		**/
		function lead_capture_uninstall() {
			  
			  //if( ! defined( 'WP_UNINSTALL_PLUGIN' ) )
			  //exit();
		}
	}
}
/* Instantiate the Lead Capture class */
$ObSotre = new ClassLeadCapture();