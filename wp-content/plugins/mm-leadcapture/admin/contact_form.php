<?php
/**
 * Setting section of leadcapture Plugin.
 * 
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit;// Exit if accessed directly
if(isset($_REQUEST['action']) && $_REQUEST['action']=='delete'){
	$post_id=$_REQUEST['post'];
	wp_delete_post($post_id);
	echo "<script type='text/javascript'>window.location='admin.php?page=contact_form';</script>";
}
require_once(LEAD_CAPTURE_DIR.'/classes/class_list_table.php');
//Prepare Table of elements
$wp_list_table = new LC_Contact_Form_List_Table();
$wp_list_table->prepare_items();
//Table of elements
$wp_list_table->display();
?>