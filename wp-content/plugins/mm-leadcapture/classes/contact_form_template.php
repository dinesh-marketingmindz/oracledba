<?php

class ContactForm8Template {
	public static function get_default( $prop = 'form' ) {
		if ( 'form' == $prop ) {
			$template = self::form();
		} elseif ( 'mail' == $prop ) {
			$template = self::mail();
		} elseif ( 'mail_2' == $prop ) {
			$template = self::mail_2();
		} elseif ( 'messages' == $prop ) {
			$template = self::messages();
		} elseif ( 'style_form' == $prop ) {
			$template = self::style_form();
		}else {
			$template = null;
		}
		return $template;
	}

	public static function form() {
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			$postId=$_REQUEST["post"];
			$template = get_post_meta($postId, '_form', true);
		}else{
			$template =
				'<p>' . __( 'Your Name' )
				. ' ' . __( '(required)' ) . '<br />' . "\n"
				. '    [text* your-name] </p>' . "\n\n"
				. '<p>' . __( 'Your Email' )
				. ' ' . __( '(required)' ) . '<br />' . "\n"
				. '    [email* your-email] </p>' . "\n\n"
				. '<p>' . __( 'Subject' ) . '<br />' . "\n"
				. '    [text your-subject] </p>' . "\n\n"
				. '<p>' . __( 'Your Message' ) . '<br />' . "\n"
				. '    [textarea your-message] </p>' . "\n\n"
				. '<p>[submit '. __( 'Save' ) .' "' . __( 'Send' ) . '" class:'.__( 'mybutton' ).']</p>';
				//. '<p>[submit '. __( 'Save' ) .' "' . __( 'Send' ) . '" ]</p>';
				//. '<p>[submit '. __( 'Save' ) .' value"' . __( 'Send' ) . '" style:'.__( $button_style).']</p>';
		}
		return $template;
	}
	public static function style_form() {
		
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			$postId=$_REQUEST["post"];
			echo $template = get_post_meta($postId, '_styleform', true);
		}
		return $template;
	}

	public static function mail() {
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			$postId=$_REQUEST["post"];
			$template = get_post_meta($postId, '_mail',true);
			$template=unserialize($template);
		}else{
			$template = array(
				'mail_subject' => '[your-subject]',
				'mail_sender' => sprintf( '[your-name] <%s>', self::from_email() ),
				'mail_body' =>
					sprintf( __( 'From: %s' ),
						'[your-name] <[your-email]>' ) . "\n"
					. sprintf( __( 'Subject: %s' ),
						'[your-subject]' ) . "\n\n"
					. __( 'Message Body:' )
						. "\n" . '[your-message]' . "\n\n"
					. '--' . "\n"
					. sprintf( __( 'This e-mail was sent from a contact form on %1$s (%2$s)' ), get_bloginfo( 'name' ), get_bloginfo( 'url' ) ),
				'mail_recipient' => get_option( 'admin_email' ),
				'mail_additional-headers' => 'Reply-To: [your-email]',
				'mail_attachments' => '',
				'mail_use-html' => 0,
			);
		}
		return $template;
	}

	public static function mail_2() {
		$template = array(
			'active' => false,
			'subject' => '[your-subject]',
			'sender' => sprintf( '%s <%s>',
				get_bloginfo( 'name' ), self::from_email() ),
			'body' =>
				__( 'Message Body:' )
					. "\n" . '[your-message]' . "\n\n"
				. '--' . "\n"
				. sprintf( __( 'This e-mail was sent from a contact form on %1$s (%2$s)' ), get_bloginfo( 'name' ), get_bloginfo( 'url' ) ),
			'recipient' => '[your-email]',
			'additional_headers' => sprintf( 'Reply-To: %s',
				get_option( 'admin_email' ) ),
			'attachments' => '',
			'use_html' => 0,
			'exclude_blank' => 0 );
		return $template;
	}

	public static function from_email() {
		$admin_email = get_option( 'admin_email' );
		$sitename = strtolower( $_SERVER['SERVER_NAME'] );
		if ( 'localhost' == $sitename ) {
			return $admin_email;
		}
		if ( substr( $sitename, 0, 4 ) == 'www.' ) {
			$sitename = substr( $sitename, 4 );
		}
		if ( strpbrk( $admin_email, '@' ) == '@' . $sitename ) {
			return $admin_email;
		}
		return 'wordpress@' . $sitename;
	}

	public static function messages() {
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			$postId=$_REQUEST["post"];
			$messages = get_post_meta($postId, '_message', true);
			$messages=unserialize($messages);
		}else{
			$messages = array();
			$messages=cf8_messages();
		}
		return $messages;
	}
}



function cf8_messages() {
	$messages = array(
		'mail_sent_ok' => array(
			'description'
				=> __( "Sender's message was sent successfully" ),
			'default'
				=> __( 'Your message was sent successfully. Thanks.' )
		),
		'mail_sent_ng' => array(
			'description'
				=> __( "Sender's message was failed to send" ),
			'default'
				=> __( 'Failed to send your message. Please try later or contact the administrator by another method.' )
		),
		'validation_error' => array(
			'description'
				=> __( "Validation errors occurred" ),
			'default'
				=> __( 'Validation errors occurred. Please confirm the fields and submit it again.' )
		),
		'spam' => array(
			'description'
				=> __( "Submission was referred to as spam" ),
			'default'
				=> __( 'Failed to send your message. Please try later or contact the administrator by another method.' )
		),
		'accept_terms' => array(
			'description'
				=> __( "There are terms that the sender must accept" ),
			'default'
				=> __( 'Please accept the terms to proceed.' )
		),
		'invalid_required' => array(
			'description'
				=> __( "There is a field that the sender must fill in" ),
			'default'
				=> __( 'Please fill the required field.' )
		)
	);
	return $messages;
}



?>