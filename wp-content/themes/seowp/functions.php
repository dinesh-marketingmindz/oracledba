<?php
/**
 * The template for displaying the footer.
 *
 * -------------------------------------------------------------------
 *
 * DESCRIPTION:
 *
 * This file used to call almost all other PHP scripts and libraries needed.
 * The file contains some of the primary functions to set main theme settings.
 * All bundled plugins are also called from here using TGMPA class.
 *
 * @package    SEOWP WordPress Theme
 * @author     Vlad Mitkovsky <info@lumbermandesigns.com>
 * @copyright  2014 Lumberman Designs
 * @license    http://themeforest.net/licenses
 * @link       http://themeforest.net/user/lumbermandesigns
 *
 * -------------------------------------------------------------------
 *
 * Send your ideas on code improvement or new hook requests using
 * contact form on https://themeforest.net/user/lumbermandesigns
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * -----------------------------------------------------------------------------
 * Theme PHP scripts and libraries includes
 */

$theme_dir = get_template_directory();
$plugins_integration_dir = $theme_dir . '/inc/plugins-integration';

require_once ( ABSPATH . 'wp-admin/includes/plugin.php' );           // 1
require_once ( $theme_dir . '/design/functions-themedefaults.php'); // 2
require_once ( $theme_dir . '/inc/functions-extras.php' );         // 3

// Plugin integrations
require_once ( $plugins_integration_dir . '/class-tgmpa.php');           // 4
require_once ( $plugins_integration_dir . '/metaboxes.php' );           // 6
require_once ( $plugins_integration_dir . '/livecomposer.php' );       // 7

require_once ( $plugins_integration_dir . '/megamainmenu.php' );     // 9
require_once ( $plugins_integration_dir . '/masterslider.php' );    // 10
require_once ( $plugins_integration_dir . '/rankie.php' ); 		    // 10.1
require_once ( $plugins_integration_dir . '/nex-forms.php' );     // 10.2
require_once ( $plugins_integration_dir . '/ninja-forms.php' );  // 10.3

require_once ( $theme_dir . '/inc/customizer/headerpresets.class.php');      // 11
require_once ( $theme_dir . '/inc/customizer/customized-css.php');          // 12
require_once ( $theme_dir . '/inc/functions-themeinstall.php' );           // 13
require_once ( $theme_dir . '/inc/functions-ini.php' );                   // 14
require_once ( $theme_dir . '/all-icons-page.php');                      // 15
require_once ( $theme_dir . '/inc/customizer/customizer.php' );         // 16
require_once ( $theme_dir . '/inc/importer/widgets-importer.php' );    // 17
require_once ( $theme_dir . '/inc/functions-nopluginsinstalled.php' );// 18
require_once ( $theme_dir . '/inc/wp-updates-theme.php' );           // 19
require_once ( $theme_dir . '/inc/functions-big-updates.php' );     // 20
require_once ( $theme_dir . '/inc/beacon-helper/beacon.php' );     // 21

/**
 *  1. Need this to have is_plugin_active()
 *  2. Import theme default settings ( make sure theme defaults are the first
 *     among other files to include !!! )
 *  3. Some extra functions that can be used by any of theme template files
 *
 *  4. TGMP class for plugin install and updates (modified http://goo.gl/frBZcL)
 *  5. ---
 *  6. Framework used to create custom meta boxes
 *  7. LiveComposer plugin integration
 *  9. Mega Main Menu plugin integration
 *  10. Master Slider plugin integration
 *  10.1. WordPress Rankie plugin integration
 *  10.2. NEX-Forms plugin integration
 *  10.3. Ninja-Forms plugin integration
 *
 *  11. Header design presets class (used in Theme Customizer)
 *  12. Custom CSS generator (based on Theme Customizer options)
 *  13. On theme activation installation functions
 *  14. Functions called on theme initialization
 *  15. Creates admin page used by modal window with all custom icons listed
 *  16. All Theme Customizer magic
 *
 *  17. Widget Importer Functions (based on Widget_Importer_Exporter plugin)
 *  18. Functions to be used when not all required plugins installed
 *  19. WPUpdates Theme Updater Class for automatic theme updates
 *  20. Custom functions that do some stuff during complex/big theme updates
 *  21. Beacon Helper and Documentation search tool by Help Scout
 */

/**
* ----------------------------------------------------------------------
* Setup some of the theme settings
*
* http://codex.wordpress.org/Plugin_API/Action_Reference
*
* Generally used to initialize theme settings/options.
* This is the first action hook available to themes,
* triggered immediately after the active theme's functions.php
* file is loaded. add_theme_support() should be called here,
* since the init action hook is too late to add some features.
* At this stage, the current user is not yet authenticated.
*/

add_action( 'after_setup_theme', 'lbmn_setup' ); // Bind theme setup callback

if ( ! function_exists( 'lbmn_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which runs
	 * before the init hook. The init hook is too late for some features, such as indicating
	 * support post thumbnails.
	 */
	function lbmn_setup() {
		load_theme_textdomain( 'lbmn', get_template_directory() . '/languages' );
		// Make theme available for translation

		// add_theme_support( 'post-formats', array( 'aside', 'quote', 'link' ) );
		// Enable support for Post Formats

		add_theme_support( 'post-thumbnails' );
		// Enable support for Post Thumbnails on posts and pages

		add_theme_support( 'automatic-feed-links' );
		// Add default posts and comments RSS feed links to head

		// This theme uses wp_nav_menu()
		// Here we define menu locations available
		register_nav_menus( array(
			'topbar'	=> __( 'Top Bar', 'lbmn' ),
			'header-menu'	=> __( 'Header', 'lbmn' ),
			// Please note: Mobile off-canvas menu is widget area no menu locaiton
		) );
	}

endif; // lbmn_setup


/**
 * ------------------------------------------------------------------------------
 * Register the required plugins for this theme.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */

define( 'THEME_LC_VER', '1.2.4.1.0.6.1' );
// Latest compatible Live Composer version
// It overrides the LC dashboard updates feature to make sure LC do not
// update itself until we test and permit it


// Delete the redirect transient to not allow Ninja Forms to redirect
// theme users to their welcome page ont he first plugin install
delete_transient( '_nf_activation_redirect' );


add_action( 'tgmpa_register', 'lbmn_register_required_plugins' );
function lbmn_register_required_plugins() {
	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	// there is a bug in tgmpa that prevents me from using 'default_path' in config
	$plugins_paths = LBMN_INSTALLER . LBMN_PLUGINS;
	//get_template_directory_uri() . '/inc/plugins-integration/plugin-installables/';
	$plugins = array(

		// Include amazing 'Live Composer' plugin pre-packaged with a theme
		// http://codecanyon.net/item/live-composer-frontend-wordpress-page-builder/6594028?ref=lumbermandesigns
		array(
			'name'     				=> 'Live Composer',
			'slug'     				=> 'ds-live-composer',
			'source'   				=> $plugins_paths . 'live-composer-page-builder.1.2.4.1.0.6.1.zip',
			'required' 				=> true,
			'version' 				=> '1.2.4.1.0.6.1', // make sure to update THEME_LC_VER constant too
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Mega Main Menu' plugin pre-packaged with a theme
		// http://codecanyon.net/item/mega-main-menu-wordpress-menu-plugin/6135125?ref=lumbermandesigns
		array(
			'name'     				=> 'Mega Main Menu',
			'slug'     				=> 'mega_main_menu',
			'source'   				=> $plugins_paths . 'mega-main-menu.20.1.1.zip',
			'required' 				=> true,
			'version' 				=> '20.1.1',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Meta Box' plugin pre-packaged with a theme
		// http://wordpress.org/plugins/meta-box/
		array(
			'name'     				=> 'Meta Box',
			'slug'     				=> 'meta-box',
			'required' 				=> true,
			'version' 				=> '',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'WPFW - Menus Management' plugin pre-packaged with a theme
		// http://codecanyon.net/item/wordpress-menus-management/7814552?ref=lumbermandesigns
		array(
			'name'     				=> 'WPFW - Menus Management',
			'slug'     				=> 'wpfw_menus_management',
			'source'   				=> $plugins_paths . 'wpfw_menus_management.1.5.zip',
			'required' 				=> true,
			'version' 				=> '1.5',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'MasterSlider' plugin pre-packaged with a theme
		// http://codecanyon.net/item/master-slider-wordpress-responsive-touch-slider/7467925?ref=lumbermandesigns
		array(
			'name'     				=> 'MasterSlider',
			'slug'     				=> 'masterslider',
			'source'   				=> $plugins_paths . 'masterslider.20.25.4.zip',
			'required' 				=> false,
			'version' 				=> '20.25.4',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Easy Social Share Buttons for WordPress' plugin pre-packaged with a theme
		// http://codecanyon.net/item/easy-social-share-buttons-for-wordpress/6394476?ref=lumbermandesigns
		array(
			'name'     				=> 'Easy Social Share Buttons for WordPress',
			'slug'     				=> 'easy-social-share-buttons3',
			'source'   				=> $plugins_paths . 'easy-social-share-buttons.30.2.4.zip',
			'required' 				=> false,
			'version' 				=> '30.2.4',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Ninja Forms' plugin pre-packaged with a theme
		// https://wordpress.org/plugins/ninja-forms/
		array(
			'name'     				=> 'Ninja Forms',
			'slug'     				=> 'ninja-forms',
			'required' 				=> false,
			'version' 				=> '',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Ninja Forms - Layout Master' premium plugin pre-packaged with our theme
		// http://codecanyon.net/item/ninja-forms-layout-master/9372347/?ref=lumbermandesigns
		array(
			'name'     				=> 'Ninja Forms - Layout Master',
			'slug'     				=> 'ninja-forms-layout-master',
			'source'   				=> $plugins_paths . 'ninja-forms-layout-master.1.7.1.zip',
			'required' 				=> false,
			'version' 				=> '1.7.1',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Ninja Forms Newsletter Opt-ins' premium plugin pre-packaged with our theme
		// http://codecanyon.net/item/ninja-forms-newsletter-optins/10789725/?ref=lumbermandesigns
		array(
			'name'     				=> 'Ninja Forms MailChimp Opt-ins',
			'slug'     				=> 'ninja-forms-mailchimp-optins',
			'source'   				=> $plugins_paths . 'ninja-forms-mailchimp-optins.1.1.0.zip',
			'required' 				=> false,
			'version' 				=> '1.1.0',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Ninja Forms PayPal Standard Payment Gateway' premium plugin pre-packaged with our theme
		// http://codecanyon.net/item/ninja-forms-paypal-standard-payment-gateway/10047955/?ref=lumbermandesigns
		array(
			'name'     				=> 'Ninja Forms PayPal Standard Payment Gateway',
			'slug'     				=> 'ninja-forms-paypal-standard',
			'source'   				=> $plugins_paths . 'ninja-forms-paypal-standard.1.1.2.zip',
			'required' 				=> false,
			'version' 				=> '1.1.2',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

		// Include 'Google Maps Add-On For Live Composer' premium plugin pre-packaged with our theme
		// http://codecanyon.net/item/google-maps-addon-for-live-composer/10653749/?ref=lumbermandesigns
		array(
			'name'     				=> 'Live Composer Add-On - Google Maps',
			'slug'     				=> 'sklc-addon-googlemaps',
			'source'   				=> $plugins_paths . 'sklc-addon-googlemaps.1.1.1.zip',
			'required' 				=> false,
			'version' 				=> '1.1.1',
			'force_activation' 	=> false,
			'force_deactivation' => false,
			'external_url' 		=> '',
		),

	);

	/**
	 * Array of configuration settings.
	 */
	$config = array(
		'domain'       		=> 'lbmn',         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '', // Default absolute path to pre-packaged plugins
		// 'parent_slug' 	=> 'themes.php', 				// Default parent menu slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
		'message' 			   => '',							// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       	=> __( 'Install Required Plugins', 'lbmn' ),
			'menu_title'                       	=> __( 'Install Plugins', 'lbmn' ),
			'installing'                       	=> __( 'Installing Plugin: %s', 'lbmn' ), // %1$s = plugin name
			'oops'                             	=> __( 'Something went wrong with the plugin API.', 'lbmn' ),
			'notice_can_install_required'     	=> _n_noop( 'This theme requires the following plugin (the plugin is already included, you need only to install/activate it): %1$s.', 'This theme requires the following plugins (the plugins are already included, you need only to install/activate them): %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'		=> _n_noop( 'This theme recommends the following plugin (the plugin is already included, you need only to install/activate it): %1$s.', 'This theme recommends the following plugins (the plugins are already included, you need only to install/activate them): %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'  				=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    	=> _n_noop( 'The following required plugin is installed but inactive: %1$s.', 'The following required plugins are installed but inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'	=> _n_noop( 'The following recommended plugin is installed but inactive: %1$s.', 'The following recommended plugins are installed but inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 				=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 					=> _n_noop(
                    '<br/>Important!<br/>The following premium plugin is ready to be updated: <br /> %1$s.',
                    '<br/>Important!<br/>The following premium plugins are ready to be updated: <br /> %1$s.',
                    'tgmpa'
                ),
			'notice_cannot_update' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 					  		=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'                           	=> __( 'Return to Required Plugins Installer', 'lbmn' ),
			'plugin_activated'                 	=> __( 'Plugin activated successfully.', 'lbmn' ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', 'lbmn' ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa( $plugins, $config );
}

/**
 * ----------------------------------------------------------------------
 * WPUpdates Theme Updater Class Initialization
 * Automatic theme updates
 */

$license_key = get_option ('lbmn_purchase_code');

new WPUpdatesThemeUpdater_1553( 'https://wp-updates.com/api/2/theme', basename( get_template_directory() ), $license_key  );

/**
 * ----------------------------------------------------------------------
 * Save updating versions history
 */


// add_action( 'current_screen', 'lbmn_theme_version_control' );
function lbmn_theme_version_control( $current_screen ) {
	if ( 'themes' == $current_screen->base ) {
		// Proceed only if 'themes' admin screen loaded
		$lbmn_theme = wp_get_theme();
		$curent_theme_ver = $lbmn_theme->get( 'Version' );
		$theme_ver_log = get_option( 'lbmn_theme_updates_log');

		if ( ! $theme_ver_log ) {
			$theme_ver_log = array();
		}

		// This code marks the very first theme installation
		// when no theme version control were available
		if ( get_option( LBMN_THEME_NAME . '_basic_setup_done') && ! in_array('1.0.1', $theme_ver_log) ) {
			array_unshift($theme_ver_log, '1.0.1');
			update_option( 'lbmn_theme_updates_log', $theme_ver_log);
		}

		if ( get_option( LBMN_THEME_NAME . '_basic_setup_done') ) {
			update_option( LBMN_THEME_NAME . '_basic_config_done', true);
		}

		if ( ! in_array($curent_theme_ver, $theme_ver_log) ) {
			array_unshift($theme_ver_log, $curent_theme_ver);
			update_option( 'lbmn_theme_updates_log', $theme_ver_log);
		}

		if ( get_option( LBMN_THEME_NAME . '_basic_config_done') && !defined('LBMN_THEME_CONFUGRATED') ) {
			define ('LBMN_THEME_CONFUGRATED', true);
		}
	}
}
/*
delete_option( LBMN_THEME_NAME . '_basic_setup_done');
delete_option( 'lbmn_theme_updates_log' );
*/
/**
 * ----------------------------------------------------------------------
 * Output theme debug information in HTML including template file name.
 * Must be 'false' if you finished website development.
 */

define('LBMN_THEME_DEBUG', false);

/**
 * ----------------------------------------------------------------------
 * Alternative to define( 'SCRIPT_DEBUG', true ) in wp-config.php
 */

define('LBMN_SCRIPT_DEBUG', false);

// true – original theme JavaScripts outputted
// false - minimized theme JavaScripts outputted

/**
* ----------------------------------------------------------------------
* Theme debuging helpers
*/

function lbmn_debug($var) {
	echo '<!-- debug start --><pre>';
	print_r($var);
	echo '</pre> <!-- debug end -->';
}

/**
 * Send debug code to the Javascript console
 */
function lbmn_debug_console($data) {
	if(is_array($data) || is_object($data)) {
		echo("<script>console.info('PHP: ".json_encode($data)."');</script>");
	} else {
		echo("<script>console.info('PHP: ".$data."');</script>");
	}
}

if ( !defined('LBMN_THEME_CONFUGRATED') ) {
	if( get_option( LBMN_THEME_NAME . '_basic_config_done') ) {
		define ('LBMN_THEME_CONFUGRATED', true);
	} else {
		define ('LBMN_THEME_CONFUGRATED', false);
	}
}

/**
 * ----------------------------------------------------------------------
 * WPML integration
 */

add_action( 'current_screen', 'lbmn_wpml_integration' );
function lbmn_wpml_integration( $current_screen ) {

	if ( $current_screen->id == 'wpml-string-translation/menu/string-translation' ) {

		// Register single strings form the Customizer for WPML translation in WP > WPML > Strings Translation
		// @reference https://wpml.org/wpml-hook/wpml_register_string_for_translation/
		do_action( 'wpml_register_single_string', 'Theme Customizer', 'Notification panel (before header) – Message', get_theme_mod( 'lbmn_notificationpanel_message', LBMN_NOTIFICATIONPANEL_MESSAGE_DEFAULT )  );
		do_action( 'wpml_register_single_string', 'Theme Customizer', 'Notification panel (before header) – URL', get_theme_mod( 'lbmn_notificationpanel_buttonurl', LBMN_NOTIFICATIONPANEL_BUTTONURL_DEFAULT )  );

		do_action( 'wpml_register_single_string', 'Theme Customizer', 'Call to action (before footer) – Message', get_theme_mod( 'lbmn_calltoaction_message', LBMN_CALLTOACTION_MESSAGE_DEFAULT ) );
		do_action( 'wpml_register_single_string', 'Theme Customizer', 'Call to action (before footer) – URL', get_theme_mod( 'lbmn_calltoaction_url', LBMN_CALLTOACTION_URL_DEFAULT ) );
	}
}

/**
 * ----------------------------------------------------------------------
 * WPML integration - RTL Menu
 */

add_filter( 'mmm_container_class', 'mmm_container_class_rtl', 10, 2 );
function mmm_container_class_rtl( $value='', $predefined_classes ){

	$styling_classes = '';

	if ( apply_filters( 'wpml_is_rtl', NULL ) ) {
	    $styling_classes = 'language_direction-rtl';
	} else {
	    $styling_classes = 'language_direction-ltr';
	}
	
	return $styling_classes;
}

/**
 * ----------------------------------------------------------------------
 * Add lang code in body class
 */

function wpml_lang_body_class( $classes ) {

	if ( defined( "ICL_LANGUAGE_CODE" ) ) {
	    $classes[] = 'current_language_' . ICL_LANGUAGE_CODE;
	    return $classes;
    } else{
    	return $classes;
    }
}
add_filter( 'body_class','wpml_lang_body_class' );

/**
 * ----------------------------------------------------------------------
 * WPML integration - Ninja Forms
 */

add_action( 'current_screen', 'lbmn_wpml_integration_ninja_forms' );
function lbmn_wpml_integration_ninja_forms( $current_screen ) {

	if ( $current_screen->id == 'wpml-string-translation/menu/string-translation' ) {

		$all_fields = ninja_forms_get_all_fields();

		$i = 0;

		foreach ($all_fields as $value) {

			//Label Inside
			if ( $all_fields[$i]['data']['label_pos'] == 'inside' and $all_fields[$i]['data']['req'] == 1 ) {
				$settings = apply_filters( "ninja_forms_settings", get_option( "ninja_forms_settings" ) );
				do_action( 'wpml_register_single_string', 'Ninja Forms Plugin', 'Label Inside - ' . $all_fields[$i]['data']['label'] . ' ' . strip_tags($settings['req_field_symbol']), $all_fields[$i]['data']['label'] . ' ' . strip_tags($settings['req_field_symbol']) );
			}

			//Label
	   		do_action( 'wpml_register_single_string', 'Ninja Forms Plugin', 'Label - ' . $all_fields[$i]['data']['label'], $all_fields[$i]['data']['label'] );

	   		//Default label
	   		do_action( 'wpml_register_single_string', 'Ninja Forms Plugin', 'Placeholder - ' . $all_fields[$i]['data']['placeholder'], $all_fields[$i]['data']['placeholder'] );

	   		//Description Text
	   		do_action( 'wpml_register_single_string', 'Ninja Forms Plugin', 'Description Text - ' . $all_fields[$i]['data']['desc_text'], $all_fields[$i]['data']['desc_text'] );

	   		//MailChimp
	   		do_action( 'wpml_register_single_string', 'Ninja Forms Plugin', 'MailChimp - ' . $all_fields[$i]['data']['optin_mailchimp_checkbox_text'], $all_fields[$i]['data']['optin_mailchimp_checkbox_text'] );

	   		$i++;
		}

	}
}

function lbmn_wpml_integration_ninja_forms_fields( $data, $field_id ) {

	if ( ! empty( $data ) ) {

		// Label
		if ( isset($data['label']) ) {
			$data['label'] = apply_filters( 'wpml_translate_single_string', $data['label'], 'Ninja Forms Plugin', 'Label - ' . $data['label'] );
		}

		//Default label
		if ( isset($data['default_value']) ) {
			$data['default_value'] = apply_filters( 'wpml_translate_single_string', $data['default_value'], 'Ninja Forms Plugin', 'Default label - ' . $data['default_value'] );
		}

		//Placeholder
		if ( isset($data['placeholder']) ) {
			$data['placeholder'] = apply_filters( 'wpml_translate_single_string', $data['placeholder'], 'Ninja Forms Plugin', 'Placeholder - ' . $data['placeholder'] );
		}

		//Description Text
		if ( isset($data['desc_text']) ) {
			$data['desc_text'] = apply_filters( 'wpml_translate_single_string', $data['desc_text'], 'Ninja Forms Plugin', 'Description Text - ' . $data['desc_text'] );
		}

		//MailChimp
		if ( isset($data['optin_mailchimp_checkbox_text']) ) {
			$data['optin_mailchimp_checkbox_text'] = apply_filters( 'wpml_translate_single_string', $data['optin_mailchimp_checkbox_text'], 'Ninja Forms Plugin', 'MailChimp - ' . $data['optin_mailchimp_checkbox_text'] );
		}
	}

	return $data;

}
add_filter( 'ninja_forms_field', 'lbmn_wpml_integration_ninja_forms_fields', 10, 2 );
//require ('functions_mm.php');

//ip tracking script code
add_action('wp_loaded', 'get_ip');
function get_ip()
{
    global $wpdb;
    
    $ip_update = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}ip_report` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `ip` VARCHAR(50) NOT NULL,
    `name` VARCHAR(100) NOT NULL, 
    `email` VARCHAR(50) NOT NULL, 
    `contact` VARCHAR(50) NOT NULL,    
    `address` VARCHAR(100) NOT NULL,
    `city` VARCHAR(50) NOT NULL,    
    `country` VARCHAR(50) NOT NULL,      
    `createdtime` DATE NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='All transaction table' ";
    
    $wpdb->query($ip_update);
    $user_ip = get_client_ip();
    if ($user_ip == 'UNKNOWN') {
        exit();
    }
    $ip_check = "Select * from `{$wpdb->prefix}ip_report` WHERE ip = '" . $user_ip . "'";
    $wpdb->query($ip_check);
    $ip_getit = $wpdb->num_rows;
    
    if ($ip_getit === 0) {
        $location     = file_get_contents('https://www.whoisxmlapi.com/whoisserver/WhoisService?domainName=' . $user_ip . '&username=marketingmindz&password=zero123&outputFormat=json');
        $data         = json_decode($location, true);
        $company_name = $data['WhoisRecord']['registrant']['name'];
        $address      = $data['WhoisRecord']['registrant']['city'];
        $city         = $data['WhoisRecord']['registrant']['postalCode'];
        $country      = $data['WhoisRecord']['registrant']['country'];
        $rawText      = $data['WhoisRecord']['registrant']['rawText'];
        //---------email---//
        $meail_mixed  = explode("Email:", $rawText);
        $email        = explode('descr', $meail_mixed[1]);
        $email_id     = $email[0];
        //---------phone---//
        $meail_mixed2 = explode("Phone:", $rawText);
        
        if (strpos($meail_mixed2[1], "descr")) {
            $phone   = explode('descr', $meail_mixed2[1]);
            $contact = $phone[0];
        } else {
            $phone   = explode('country', $meail_mixed2[1]);
            $contact = $phone[0];
            
        }
        
        
        $insert_ip = "INSERT INTO `{$wpdb->prefix}ip_report` (`ip`, `name`, `email`, `contact`,`address`, `city`, `country`,`createdtime` )VALUES ('" . $user_ip . "','" . $company_name . "','" . $email_id . "','" . $contact . "','" . $address . "' , '" . $city . "','" . $country . "', CURDATE())";
        $wpdb->query($insert_ip);
        
    }
}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


// add once 10 minute interval to wp schedules
function new_interval($interval)
{
    
    $interval['days_1'] = array(
        'interval' => 60 * 24 * 60,
        'display' => 'Once in a day'
    );
    return $interval;
    
}

add_filter('cron_schedules', 'new_interval');

add_action('wp', 'prefix_setup_schedule');
/**
 * On an early action hook, check if the hook is scheduled - if not, schedule it.
 */
function prefix_setup_schedule()
{
    if (!wp_next_scheduled('prefix_daily_event')) {
        
        wp_schedule_event(time('7:30'), 'days_1', 'prefix_daily_event');
    }
}

add_action('prefix_daily_event', 'prefix_do_this_daily');
/**
 * On the scheduled action hook, run a function.
 */


function prefix_do_this_daily()
{
    
    global $wpdb;
    $today       = date("Y-m-d");
    $sql         = "Select * from `{$wpdb->prefix}ip_report` WHERE createdtime ='" . $today . "'";
    $thepost     = $wpdb->get_results($sql);
    $check_id    = 'suman@marketingmindz.in,vinod@marketingmindz.in';
    $to          = $check_id;
    $domain_name = $_SERVER['SERVER_NAME'];
    $subject     = "ips track on your domain" . $domain_name;
    $ids         = get_option('mailserver_url');
    $headers     = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-Type: text/html; charset=UTF-8';
    $headers .= 'From: <' . $ids . '>' . "\r\n";
    $message = " ";
    $message .= 'Hello, ' . "<b>" . $domain_name . "</b>" . ' These IPs currently visit your website and his other information is provided below:';
    $message .= "<table border='1'>
        <tr>
        <th>visited ip on your domain </th>
        <th>Oraganization name</th>
        <th>Email</th>
        <th>Contact</th>
        <th>Address</th>
        <th>city</th>
        <th>country</th>
            
        </tr>";
    
    foreach ($thepost as $singlepost) {
        $message .= "<tr>
        <td>" . $singlepost->ip . "</td>        
        <td>" . $singlepost->name . "</td>
        <td>" . $singlepost->email . "</td>
        <td>" . $singlepost->contact . "</td>
        <td>" . $singlepost->address . "</td>
        <td>" . $singlepost->city . "</td>
        <td>" . $singlepost->country . "</td>
        </tr>
        ";
    }
    $message .= "</table>";
    
    $mailResult = false;
    $mailResult = wp_mail($to, $subject, $message, $headers);
}
/** Contact from 7  data sync with crm */
add_action("wpcf7_before_send_mail", "wpcf7_do_something_else");  
function wpcf7_do_something_else($cf7) {
    // get the contact form object
    $wpcf = WPCF7_ContactForm::get_current();
    $wpcf->skip_mail = false; 
    
    $submission = WPCF7_Submission::get_instance();
    if ( $submission ) {
    $posted_data = $submission->get_posted_data();
    }
       if(isset($posted_data['tel-746']) && !empty($posted_data['tel-746'])){
             $posted_data['tel-746'] = $posted_data['tel-746']; 
       }else{
              $posted_data['tel-746'] = $posted_data['contact'];
       }
       if(isset($posted_data['your-name']) && !empty($posted_data['your-name'])){
             $posted_data['your-name'] = $posted_data['your-name']; 
       }else{
              $posted_data['your-name'] = $posted_data['your-name'];
       }
       if(isset($posted_data['your-email']) && !empty($posted_data['your-email'])){
             $posted_data['your-email'] = $posted_data['your-email']; 
       }else{
              $posted_data['your-email'] = $posted_data['your-email'];
       }
       if(isset($posted_data['last-name']) && !empty($posted_data['last-name'])){
             $posted_data['last-name'] = $posted_data['last-name']; 
       }else{
              $posted_data['last-name'] = '';
       }

        //update_option('vinod', $submission);
        $fname = $posted_data['your-name'];     
        $email = $posted_data['your-email'];
        $phone = $posted_data['tel-746'];
        $lname = $posted_data['last-name'];        
        $lengthphone=strlen((string) $phone);
        $lead_source = get_option('siteurl');
        $leadDate = date("D M d, Y G:i");       
                 
        $lc_crm_database_name = get_option('lc_crm_database_name'); 
        $lc_crm_database_username = get_option('lc_crm_database_username'); 
        $lc_crm_database_password = get_option('lc_crm_database_password');
        $lc_crm_database_host = get_option('lc_crm_database_host');
        /*crm database connectivity */   
        $conn = mysql_connect($lc_crm_database_host, $lc_crm_database_username, $lc_crm_database_password);
        mysql_select_db($lc_crm_database_name, $conn);
         /*crm database connectivity */   

        /*crm  lead capture code*/      
        if($fname !='' && $email !='' ){
                $firstname = $fname;
                $lastname = $lname;
                $primaryphone = $phone;
                $company = '';
                $mobile = '';
                $designation = '';
                $primaryemail = $email;
                $secondaryemail = '';
                $website = '';
                $leadsource = $lead_source;
                $employee = '';
                $annualrevenue = '';    
                $street         = '';
                $fax            = '';
                $postalcode     = '';
                $city           = '';
                $pobox       = '';
                $state      = '';
                $country         = '';
                $date = date('Y-m-d H:i:s');
                $label = $firstname." ".$lastname;

                $results = mysql_query("SELECT * from vtiger_leaddetails order by leadid DESC LIMIT 0,1", $conn);
                while($arr = mysql_fetch_assoc($results))
                {
                    $lastid = $arr['leadid'];
                    
                }           
                $lastid =$lastid+1;             
                $crmid_res = mysql_query("SELECT * from vtiger_crmentity_seq",$conn);
                while($row= mysql_fetch_array($crmid_res))
                {
                     $crmid = $row['id'];                   
                }

                $crmid =$crmid+1;               
                $lead_seq_res = mysql_query("SELECT * from vtiger_modentity_num where semodule='Leads'",$conn);
                while($lead = mysql_fetch_array($lead_seq_res))
                 {
                    $lead_seq =  $lead['cur_id'];
                    $lead_prefix =  $lead['prefix'];                    

                  }
                $lead_seq = $lead_seq;              
                $vtiger_leaddetails =  "INSERT INTO `vtiger_leaddetails` SET 
                                `leadid` = '" .$crmid . "',
                                `lead_no` = '$lead_prefix".$lead_seq."',
                                `firstname` = '" .$firstname . "',
                                `lastname` = '" .$lastname . "',
                                `company` = '" .$company . "',
                                `annualrevenue` = '" .$annualrevenue . "',
                                `email` = '" .$primaryemail . "',
                                `leadsource` = '" .$leadsource . "',
                                `designation` = '" .$designation . "'";

                $vtiger_leaddetailsa = mysql_query($vtiger_leaddetails,$conn);
                $lead_seq = $lead_seq+1;
                $update_lead_seq = "update vtiger_modentity_num SET cur_id = '$lead_seq' where semodule='Leads'";
                mysql_query($update_lead_seq,$conn);
                $vtiger_leadaddress = "INSERT INTO `vtiger_leadaddress` SET 
                                `leadaddressid` = '" .$crmid . "',
                                `city` = '" .$city . "',
                                `code` = '" .$postalcode . "',
                                `state` = '" .$state . "',
                                `pobox` = '" .$pobox . "',
                                `country` = '" .$country . "',
                                `phone` = '" .$primaryphone . "',
                                `mobile` = '" .$mobile . "',
                                `fax` = '" .$fax . "',
                                `lane` = '" .$street . "'";
                                
                mysql_query($vtiger_leadaddress,$conn);                     
                $leadscf = "INSERT INTO `vtiger_leadscf` SET 
                                `leadid` = '" .$crmid . "'";
                                
                mysql_query($leadscf,$conn);
                
                $vtiger_leadsubdetails = "INSERT INTO `vtiger_leadsubdetails` SET
                                `website` = '" .$website . "',
                                `leadsubscriptionid` = '" .$crmid . "'";
                                
                mysql_query($vtiger_leadsubdetails,$conn);          
                
                $vtiger_crmentity = "INSERT INTO `vtiger_crmentity` SET 
                                `crmid` = '" .$crmid . "',
                                `smcreatorid` = 11,
                                `smownerid` = 11,
                                `modifiedby` = 11,
                                `setype` = 'Leads',
                                `createdtime` = '$date',
                                `modifiedtime` = '$date',
                                `label` = '$label',
                                `presence` = 11";
                                
                mysql_query($vtiger_crmentity,$conn);
                
                $update_crmentity_seq = "update vtiger_crmentity_seq SET id = '$crmid'";
                                
                mysql_query($update_crmentity_seq,$conn);

                 /*crm  lead capture code*/ 
                
        } else{
                echo "All Field Required";
            }
            /****/

    return $wpcf;
}
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );
function enqueue_load_fa() { 
    wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' ); 
}
function modify_jquery() {
    if (!is_admin()) {
        // comment out the next two lines to load the local copy of jQuery
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', false, '1.8.1');
        wp_enqueue_script('jquery');
 }

}
add_action('init', 'modify_jquery'); 
