<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oracledb_oracledba');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
/*define('AUTH_KEY',         'U%Y3<p]1Fg&^`dn6r6<q!]B<7C_!sG@9f77jD5qRAT2ZC)w.a9rp_gMbIkQ#@:]h');
define('SECURE_AUTH_KEY',  'kUH86VKPT0^*5.tPD$;-HBE8X0uZ~.HCJcY1!U`Okd*<>?5=@8iGwoSA.YTX-ebS');
define('LOGGED_IN_KEY',    '?TWD<Z,8/{p)Mv(or^i9.>b]yeM}@8Wv!D^vR2nxv2XWu:2H#5Q/WuPnZ:fZ2Li&');
define('NONCE_KEY',        '=|7wbS>=w!n:Y{]@&!)J==)8aj&|GJ?v.!H;k:1Bh!fV?d?bOAk0Yh?}3<Rx%w{.');
define('AUTH_SALT',        'gMea$yg|Rh~7C;;c3XY*bcBnvI <]F|P6vB<,]h4PiL;ZIs2$9f3^:T_ pM?i&@4');
define('SECURE_AUTH_SALT', '=<RF^jvlK+Wr`t)W?u}.DVjZSvFjua}Nm@Mpd0HrO!#X@!kiOs=&bQDaF@Qjf6@p');
define('LOGGED_IN_SALT',   'F~O2meoaTiGw+v:tC;^Me87SK(_ucLo>5:!WzMfib1z=DaT>$Q8)+~4q(PGXq&AQ');
define('NONCE_SALT',       'DkmIj~sN2+q!Ld[]eM =<Y1^lY344oUX@3fQCm6pv0o,l$JI(a;,bJDmw0JMEC35');*/

define('AUTH_KEY',         'Pe-as89OpmWl:D/+T-ge)IQM74okFOtuMPbwX+*P.<LM!),8QN.|v(qGv&hB=|Rh');
define('SECURE_AUTH_KEY',  '[8Hay8-o]Vdv7)e#3CqFmYDXyu=#Y+>bAsU$@+L|#U}LgpLm%Dw9+S#g&/XjM3P*');
define('LOGGED_IN_KEY',    '&.}$hd<uf1RFd/*1+-9QybOUg^D2(Q{/pCw(TM}:UFgLl3Vu`wS7&2qRaFUB]PN-');
define('NONCE_KEY',        'z7z$~yX;90qr:4L/ 0+|Um2}^4q)cgU+2XhJ0=+kizL#&OAJZtLLl*<~Q[V9.R0G');
define('AUTH_SALT',        'd ]v%L{i 4C@?-A:|i`I/?3,}v|z|b3k~o.n$-xW3U(gnR!PBR9[g{s3t6_{{#$)');
define('SECURE_AUTH_SALT', ';6>^lso9S,xA.AOMduH-KM>!NYj+{4 GY0Q;8%NiL?q-H,;YK^l^XM--@kmz~4P6');
define('LOGGED_IN_SALT',   'p;Z}(wnP>{fI{s6W7A](`(nkf-m|u6}n@~az|mi.:O<#1YcQY#NgeT7^lD+)P1fG');
define('NONCE_SALT',       'k~-)605bMe|(-L;ZH+ZFDM*9PNjjoVhv[-={tLh@!|?}V*%_mcCPsWF$-[B<RQf|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dba_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
//define('FORCE_SSL_ADMIN', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
error_reporting(0);
@ini_set(‘display_errors’, 0);
